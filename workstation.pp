/*
 * Packages
*/
/* Repos */
exec { 'repo_planetccrma':
  command => '/usr/bin/dnf -y install http://ccrma.stanford.edu/planetccrma/mirror/fedora/linux/planetccrma/22/x86_64/planetccrma-repo-1.1-3.fc22.ccrma.noarch.rpm',
  creates => '/etc/yum.repos.d/planetccrma.repo'
}
exec { 'repo_rpmfusion_free':
  command => '/usr/bin/dnf -y install http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-23.noarch.rpm',
  creates => '/etc/yum.repos.d/rpmfusion-free.repo'
}

/* Packages */
$packages = [
  /* Audio */
  'planetccrma-core',
  'qjackctl',
  'ffado',
  'ardour4',
  'ardour4-audiobackend-jack',
  'supercollider',
  'pd-extended',
  'qtractor',
  'audacity',
  'muse',
  'musescore',
  'ladspa-swh-plugins',
  'ladspa-calf-plugins',
  'ladspa-cmt-plugins',
  'ladspa-vocoder-plugins',
  'dssi-calf-plugins',
  'lv2-calf-plugins',
  'yoshimi',
  'xsynth-dssi',
  'nekobee-dssi',
  'normalize',

   /* System */
   'kernel-tools',
   'sudo',

   /* Utilities */
   'jove',
]

package { $packages:
  ensure => present,
  require => Exec['repo_planetccrma', 'repo_rpmfusion_free'],
}

/*
 * Users
 */
define musician {
  user { $name:
    ensure => present,
    groups => 'jackuser',
  }
}

$musicians = ['ostap', 'yurb']

musician { $musicians: }

